from __future__ import unicode_literals
from youtube_dl import YoutubeDL
import vlc
from youtube_search import YoutubeSearch
import os
import time
import sys
import datetime
import random
import pafy


def milForm(milliseconds,subtractH=True):
	try:
		if subtractH: millisecondos = milliseconds-3600000
		if millisecondos > 3600000:
			return datetime.datetime.fromtimestamp(millisecondos / 1000).strftime('%H:%M:%S')
		else:
			return datetime.datetime.fromtimestamp(millisecondos / 1000).strftime('%M:%S')

	except:
		millisecondos = millisecondos
		if millisecondos > 3600000:
			return datetime.datetime.fromtimestamp(millisecondos / 1000).strftime('%H:%M:%S')
		else:
			return datetime.datetime.fromtimestamp(millisecondos / 1000).strftime('%M:%S')
class YtPlayer():
	def __init__(self,quiet=False,debug=False,vlclogs=False,forceaudiostream=False):
		self.forceaudiostream = forceaudiostream
		self.quiet = quiet
		self.debug = debug
		if not vlclogs: os.environ['VLC_VERBOSE'] = '-1'
		self.vlcInstance = vlc.Instance('--no-video','vout=none')
		self.vlcPlayer = self.vlcInstance.media_player_new()
		self._volumeLvl = 100
		self.vlcPlayer.audio_set_volume(self._volumeLvl)
		self._playingUrl = None
		self.playList = []
		self.YtInfo = None
		if self.debug:
			self.quiet = False

	def pause(self):
		self.vlcPlayer.set_pause(1)
	def unpause(self):
		self.vlcPlayer.set_pause(0)
	def stop(self):
		self.vlcPlayer.stop()

	def getTimeStamp(self):
		return (milForm(self.vlcPlayer.get_time()),milForm(self.vlcPlayer.get_length()))

	def playPlaylist(self,songlist,shuffle=False):
		self.playList = self.playList + songlist
		if shuffle: random.shuffle(self.playList)
		for item in self.playList:
			self.play(item)

	def search(self,query,maxresults=3):
		self._results = YoutubeSearch(query,max_results=maxresults).to_dict()
		# self.YtInfo = self._results[0]
		foundStream = False
		if self.debug: print(f'SEARCHING FOR SEARCH TERMS {query}')
		for self.YtInfo in self._results:
			try:
				if self.forceaudiostream:
					self._streams = [pafy.new(f'http://www.youtube.com{self.YtInfo["url_suffix"]}').getbestaudio()]
				else:
					self._streams = pafy.new(f'http://www.youtube.com{self.YtInfo["url_suffix"]}').streams
				for a in self._streams:
					if self.debug: print('info:',a.notes,a.mediatype,a.extension)	
				self._playingUrl = self._streams[0].url
				if not self.quiet: print(f'playing "{self.YtInfo["title"]}" by {self.YtInfo["channel"]} [{self.YtInfo["duration"]}]')
				return self._playingUrl
			except BaseException as e:
				if not self.quiet: print(e) 
				continue


	def play(self,query,wait=True):
		self._url = self.search(query)
		if self._url:
			self._vlcPlay(self._url)
			del self._url
		else:
			return
		if wait:
			while self.vlcPlayer.is_playing():
				time.sleep(.5)

	def setVolume(self,volumenum):
		if volumenum < 0 or volumenum > 200 or volumenum == self._volumeLvl:
			return False
		else:
			if not self.quiet: print(f'volume changed to {volumenum} (was {self._volumeLvl})')
			self._volumeLvl = volumenum
			self.vlcPlayer.audio_set_volume(self._volumeLvl)

	def _vlcPlay(self,url):
		self.media = self.vlcInstance.media_new(url,'vout=none','--no-video')
		self.vlcPlayer.set_media(self.media)
		self.vlcPlayer.play()
		self.counter = 0
		while self.vlcPlayer.is_playing() == False or self.counter > 60:
			time.sleep(.05)
			self.counter += 1
		del self.counter
		self.vlcPlayer.audio_set_volume(self._volumeLvl)
