from pypresence import Presence
import time
import ytplayer
from sys import argv
import getopt
import re
import random
import distutils.util
import threading, _thread

def reInc(text,replacethis,withthis):
	return re.sub(replacethis, withthis, text, flags=re.IGNORECASE)

def inputThread(player):
	time.sleep(4)
	print('controls: [p/s/m/e/v]')
	while True:
		action = input('')
		print('\033[F\r',end='')
		if action.startswith('p'):
			player.vlcPlayer.pause()
			time.sleep(0.2)
			print(str(player.vlcPlayer.get_state()).lower().replace('state.',''),'           ')
		elif action.startswith('s'):
			player.vlcPlayer.set_position(100.0)
		elif action.startswith('m'):
			try:
				seconds = int(action.split(' ')[1])
				player.vlcPlayer.set_time(player.vlcPlayer.get_time()+(seconds)*1000)
			except:
				print('usage: m <+seconds/-seconds>')
		elif action.startswith('e'):
			_thread.interrupt_main()
		elif action.startswith('v'):
			try:
				level = action.split(' ')[1]
				player.setVolume(int(level))
			except:
				print('usage: v <volumelevel>')
def checkPlaying(player):
	state = str(player.vlcPlayer.get_state()).lower().replace('state.','')
	if state == 'playing' or state == 'paused':
		return True
	else:
		return False

if __name__ == '__main__':
	playlist = []
	volume = 100
	arguments = argv[2:]
	RPOn = True
	shuffle = False
	forceaudiostream = False
	debuglogs = False
	repeat = False
	ThreadOn = True
	opts, args = getopt.getopt(argv[1:], 'r:v:p:sadi:l')
	# print(opts)
	# print(args)
	if args: playlist = playlist + args
	for item in opts:
		if item[0] == '-v':
			volume = int(item[1])
		elif item[0] == '-l':
			repeat = True
			print(f'loop mode enabled')
		elif item[0] == '-i':
			ThreadOn = bool(distutils.util.strtobool(item[1]))
			print(f'input set to {ThreadOn}')
		elif item[0] == '-r':
			try:
				RPOn = bool(distutils.util.strtobool(item[1]))
				print(f'rich presence set to {RPOn}')
			except BaseException as e:
				print('rich presence FAILED (line 32)')
				print(e)
				RPOn = True
		elif item[0] == '-p':
			with open(item[1],'r') as f:
				playlist = playlist + list(f.read().split('\n'))
				print('playlist added')
		elif item[0] == '-s':
			print('shuffle mode enabled')
			shuffle = True
		elif item[0] == '-a':
			print('force audiostream enabled')
			forceaudiostream = True
		elif item[0] == '-d':
			debuglogs = True
	if len(playlist) == 0:
		print('no songs specified')
		exit()

	if RPOn:
		try:
			with open('client_id.txt','r') as f:
				client_id = str(f.read()) 
			RPC = Presence(client_id)
			RPC.connect()
		except BaseException as e:
			print(e)
			RPOn = False

	if RPOn:
		waito = False
	else:
		waito = True

	player = ytplayer.YtPlayer(debug=debuglogs,forceaudiostream=forceaudiostream,vlclogs=debuglogs)
	player.setVolume(volume)

	if shuffle: random.shuffle(playlist)
	firstplay = True
	if ThreadOn: threading.Thread(target=inputThread,args=(player,),daemon=True).start()
	while repeat or firstplay:
		for song in playlist:
			player.play(song,wait=waito)
			try:
				titleList = player.YtInfo.get('title','?').split(' - ')
				title = reInc(reInc(titleList[1],'(lyrics)',''),'(lyric video)','')
				artist = titleList[0]
			except:
				title=reInc(player.YtInfo.get('title','?'),player.YtInfo['channel'],'')
				artist = player.YtInfo['channel'].replace(' - Topic','')
			while checkPlaying(player) and RPOn:
				timestamp = player.getTimeStamp()
				timestamp = f'{timestamp[0]}/{timestamp[1]}'
				if RPOn: RPC.update(details=title,state=artist,large_image="icon", small_image='clock', small_text=timestamp) #large_text="Large Text Here!",
		firstplay = False
		if shuffle and repeat: random.shuffle(playlist)